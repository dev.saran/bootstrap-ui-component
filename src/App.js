import { InputField } from "./components";

function App() {
  return (
    <div className="App">
      <InputField label="Input form" placeholder="Enter name..." />
    </div>
  );
}

export default App;
